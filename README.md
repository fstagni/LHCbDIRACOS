# Making a new release


The release procedure is automated like for DIRACOS: If you push a branch with the name `rel-vXrYpZ_vUrVpW` the CI will automatically generate a release `vXrYpZ` based on DIRACOS `vUrVpW`, the `rel-*` prefix is the sign to trigger a release.


```
   git clone ssh://git@gitlab.cern.ch:7999/lhcb-dirac/LHCbDIRACOS.git
   cd LHCbDIRACOS
   git checkout -b rel-vXrYpZ_vUrVpW
   git push origin rel-vXrYpZ_vUrVpW
```


## How it works

The gitlab-ci will first run the `compile` job, which will be based on the `rel-vXrYpZ_vUrVpW` branch you just created: so it will use the specific DIRACOS version you chose, and it will generate the correct `requirements.txt` and `lhcbdiracos.json` and save them for later.
However, the `push_tag` job starts from the `master` branch, and just copy over the configuration files generated in the `compile` job. This means that although the differences are correct (because the `rel-vXrYpZ_vUrVpW` branch was done from `master`), the commit where you set the specific version of DIRACOS is lost. This is not really clean, but it makes sure that in the `master` branch will always stick to `master` of DIRACOS.

# About Oracle

Because of licenses reasons we cannot distribute the Oracle RPMs. Thus, if you want to use the `cx_Oracle` python library packaged in DIRACOS, you should install `oracle-instantclient-basic` and `oracle-instantclient-devel` packages on your server.
